# Personal .vim Plugins

My collection of Vim plugins for the projects I work with.

### Linux

```
# on Linux
$ git clone git@gitlab.com:jamesvl/vimfiles.git ~/.config/nvim

# for Vim on Windows
$ git clone git@gitlab.com:jamesvl/vimfiles.git .vim

$ cd [vim config dir]
$ git submodule init
$ git submodule update

# later updates
$ git submodule update --remote
```
